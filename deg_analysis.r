# This script models the RCperGene data and is followed by DEG analysis
# Almost same process as what is going on in ApASAM_modeling.
# So we can borrow a couple of utils funcs from ApASAM_utils.R


# source the utils specifically designed for ApA splicing analysis
source('ApASAM_utils.R')



# ================================================
# Here we don't use the "create_DGEList" func from ApASAM_utils because that one is incorporated with
# some special settings.
# So we define a clean one.
# ================================================
create_DGEList <- function(x, conditions, references)
{
  cat("creating the DGEList object to start analysis...\n")
  y <- DGEList(counts = x, group = conditions, genes = references)
  colnames(y$genes) <- c("geneSymbols")
  cat("the created DGEList object includes:\n")
  cat("---------------------counts table---------------------\n")
  print(head(y$counts))
  cat("---------------------samples list---------------------\n")
  print(head(y$samples))
  cat("--------------------- genes list----------------------\n")
  print(head(y$genes))
  y
}

args <- commandArgs(trailingOnly = TRUE)

wd = args[1]
summed_table = args[2]
cond_f = args[3]
num_samples = args[4]
coeff = args[5]
outfile = args[6]

# setwd and load the library
setwd(wd)
library(edgeR)


# load raw counts, conditions and master list
x_raw <- load_counts_from(summed_table)
x_counts <- x_raw[,2:num_samples+1]  ## extract the int counts data
masterlist <- x_raw[,1]
myconditions <- set_experimental_conditions_as(cond_f)


# create DGEList object and filter by "minCPM" and "minDP"
y <- create_DGEList(x_counts, myconditions, masterlist)
y <- screen_row_on_miniumCPM_and_minimumDP(y, 2, 3)

# normalization
cat("normalizing the remaining counts...\n")
y <- calcNormFactors(y)

# generate MDS plot under current dir
generate_MDS(y, "G-MDS.pdf")

# define your experimental design and here I am adding an intercept column
cat("generating the experimental design...\n")
design <- model.matrix(~myconditions, data = y$samples)
print(design)

# estimate the dispersion and fit model
cat("estimating NB dispersion....\n")
y <- estimateDisp(y, design, robust = TRUE)
cat("fitting glmQL model...\n")
fit <- glmQLFit(y, design, robust = TRUE)
cat("model fitting completed.")

qlf <- glmQLFTest(fit, coef = coeff)
toplist <- topTags(qlf, n=25000)
keep <- toplist$table$FDR <= 0.05 & abs(toplist$table$logFC) >= 1

write.csv(toplist[keep,], outfile)

