import sys
from collections import defaultdict,namedtuple
from operator import attrgetter


PolyA = namedtuple('PolyA', 'g_name t_id chrm start strand tp')
PolyA_Feat = namedtuple('PolyA_Feat', 'g_name t_id chrm start strand tp sameasup diffasup'
									  'sameasdown diffasdown distal proximal middle disttoup disttodown'
									  'target1 nontarget1 target2 nontarget2 target3 nontarget3')

def extract_name_from_gtf(name_field):
	ind = name_field.find('transcript_id')
	start = ind + 15
	end = name_field[start:].find(';')
	t_id = name_field[start:start+end-1]
	#print t_id
	ind = name_field.find('gene_id')
	start = ind + 11
	end = name_field[start:].find(';')
	g_name = name_field[start : start+end-1]
	return (t_id, g_name)

def extract_3p_and_5p_fromgtf(gtf_f):
	#gtf_f = open(gtffile,'r').readlines()
	fivep = open('5prime.txt', 'w')
	threep = open('3prime.txt', 'w')
	tss = open('tss.txt', 'w')
	scodon = open('stopcodon.txt','w')
	for line in gtf_f[:] :
		fds = line.split('\t')
		#print fds
		strand = fds[6]
		t_id, g_name = extract_name_from_gtf(fds[8])
		#name = t_id + "=" + g_name
		name = t_id
		if strand  == '+':
			start = fds[3]
			end = fds[4]
		else:
			start = fds[4]
			end = fds[3]
		tp = fds[2]
		if fds[2] == 'exon' :
			fivep.write(fds[0] + "\t" + start + "\t" + start + "\t" + name + "\t" + "." + "\t" + strand + "\n" )
			threep.write(fds[0] + "\t" + end + "\t" + end + "\t" + name + "\t" + "." + "\t" + strand + "\n")
		elif fds[2] == 'transcript':
			tss.write(fds[0] + "\t" + start + "\t" + start + "\t" + name + "\t" + "." + "\t" + strand + "\n")
		elif fds[2] == 'stop_codon' :
			scodon.write(fds[0] + "\t" + start + "\t" + start + "\t" + name + "\t" + "." + "\t" + strand + "\n")
	fivep.close()
	threep.close()
	tss.close()
	scodon.close()
	print " 3 prime, 5 prime, tss and stop codon files are generated \n"

def make_exon_dict(exon_file):
	exon_dict = defaultdict(list)
	Exon = namedtuple('Exon', 'chrm start end strand t_id e_num')
	for line in exon_file:
		fds = line.split()
		temp_id = fds[3]
		ind = temp_id.find('exon')
		t_id = temp_id[:ind-1]

		ind_ = temp_id[ind+6:].find('_')
		exon_num = str(temp_id[ind+6: ind_])

		this_exon = Exon(fds[0], int(fds[1]), int(fds[2]), fds[5], t_id, exon_num)
		exon_dict[t_id].append(this_exon)
	fordist_dict = defaultdict(list)

	for gene in exon_dict:
		exons = exon_dict[gene]
		if exons[0].strand == '-':
			temp = []
			tloc = 0
			for ex in exons[::-1]:
				temp.append((ex.end, tloc))
				tloc += abs(ex.end - ex.start)
		elif exons[0].strand == '+':
			temp = []
			tloc = 0
			for ex in exons:
				temp.append((ex.start, tloc))
				tloc += abs(ex.end - ex.start)
		fordist_dict[gene].append(temp)

	return fordist_dict,exon_dict


def parse_master_list(masterlist_file):
	polya_dict = defaultdict(list)
	polya_dict_m = defaultdict(list)
	for line in masterlist_file[1:]:
		fds = line.split()
		if '+' in fds[4]:
			this_strand = "+"
			this_chrm = fds[4].split('+')[0]
			this_site = fds[4].split('+')[1]
		elif '-' in fds[4]:
			this_strand = "-"
			this_chrm = fds[4].split('-')[0]
			this_site = fds[4].split('-')[1]

		this_polya = PolyA(fds[0], fds[2], this_chrm, int(this_site), this_strand, fds[3])
		polya_dict[fds[2]].append(this_polya)

	for gene in polya_dict:
		if len(polya_dict[gene]) >= 2:
			polya_dict_m[gene] = polya_dict[gene]

	return polya_dict_m

def get_loc_on_transcript(startl, site, strand):
	startl = startl[0]
	if strand == "+":
		fnd = False
		i = 0
		while i < len(startl)-1 and not fnd:
			if site < startl[i+1][0]:
				belongs_to = i+1
				fnd = True
			i+=1
		if not fnd :
			belongs_to = i+1

		this_tloc = abs(site - startl[belongs_to-1][0]) + startl[belongs_to-1][1]

	elif strand == '-':
		fnd = False
		i = 0
		while i < len(startl)-1 and not fnd:
			if site > startl[i+1][0]:
				belongs_to = i+1
				fnd = True
			i+=1

		if not fnd :
			belongs_to = i + 1

		this_tloc = abs(site - startl[belongs_to-1][0]) + startl[belongs_to-1][1]
	return this_tloc


def get_distance_for_PASs(pas1, pas2, fordist_dict):
	pas1 = PolyA._make(pas1)
	pas2 = PolyA._make(pas2)
	#print pas1
	#print pas2
	if pas1.g_name == pas2.g_name and not pas1.t_id == pas2.t_id :
		print "aha! you are doomed " + "\n"
		return

	if pas1.g_name != pas2.g_name :
		return (0,0)

	if pas1.tp == "Intron" or pas2.tp == "Intron" or pas1.tp == 'upstreamAntisense' or pas2.tp == 'upstreamAntisense':
		#if pas1.tp =="Intron" and pas2.tp == "Intron":
			#print ' we are both in introns, we need more attention' + "\n"
		return (0,0)

	if pas1.tp == pas2.tp and pas1.tp == "LastExon":
		if pas1.strand == '+' :
			dist = int(pas1.start) - int(pas2.start)
		else:
			dist = int(pas2.start) - int(pas1.start)
		#print dist
		return (abs(dist),1)

	if pas1.tp != pas2.tp or pas1.tp=='upstreamExons' or pas2.tp =='upstreamExons':
		if len(fordist_dict[pas1.t_id]) == 0:
			return (0,0)
		tloc1 = get_loc_on_transcript(fordist_dict[pas1.t_id], pas1.start, pas1.strand)
		tloc2 = get_loc_on_transcript(fordist_dict[pas1.t_id], pas2.start, pas1.strand)
		#print tloc1, tloc2
		dist = tloc1 - tloc2
		return (abs(dist), 0)


#def extract_sequence(chrm, start, strand):

def annotate(polya_dict, threep_dict, fivep_dict, dist_dict,  outf):
	outfile = open(outf, 'w')
	for gene in polya_dict:
		polyas = polya_dict[gene]
		polyas_mod = []
		#print len(polyas)
		for i in polyas:
			polyas_mod.append(PolyA._make(i))
		if polyas_mod[0].strand == '+':
			sorted(polyas_mod, key=attrgetter('start'))
		else:
			sorted(polyas_mod, key=attrgetter('start'), reverse=True)
		if len(polyas_mod) >=2 :
			for ind in range(len(polyas_mod)):
				pa = polyas_mod[ind]
				#print pa
				if ind == 0 :
					proximal = 1
					distal = 0
					middle = 0
					#print pa
					#print polyas_mod[1]
					#print get_distance_for_PASs(pa, polyas_mod[ind+1], dist_dict)
					disttodown, sametodown = get_distance_for_PASs(pa, polyas_mod[ind+1], dist_dict)
					disttoup, sametoup = 0, 0
					difftodown = abs(1-sametodown)
					difftoup = 0
				elif ind == len(polyas_mod)-1:
					distal = 1
					proximal = 0
					middle = 0
					disttodown, sametodown = 0, 0
					disttoup, sametoup = get_distance_for_PASs(pa, polyas_mod[ind-1], dist_dict)
					difftoup = abs(1-sametoup)
					difftodown = 0
				else:
					proximal, distal, middle = 0, 0, 1
					disttodown, sametodown = get_distance_for_PASs(pa, polyas_mod[ind+1], dist_dict)
					disttoup, sametoup = get_distance_for_PASs(pa, polyas_mod[ind - 1], dist_dict)
					difftoup = abs(1-sametoup)
					difftodown = abs(1-sametodown)

				if pa.tp != 'Intron':
					distto3 = find_closest_feat(fivep_dict, pa)
					distto5 = 0
				else:
					distto5 = find_closest_feat(threep_dict, pa)
					distto3 = 0
				
				'''
				if (pa.t_id + "=" + pa.chrm + str(pa.strand) + str(pa.start)) in targets1:
					target1 = 1
					nontarget1 = 0
				else:
					target1 = 0
					nontarget1 = 1
				if (pa.t_id + "=" + pa.chrm + str(pa.strand) + str(pa.start)) in targets2:
					target2 = 1
					nontarget2 = 0
				else:
					target2 = 0
					nontarget2 = 1

				if (pa.t_id + "=" + pa.chrm + str(pa.strand) + str(pa.start)) in targets3:
					target3 = 1
					nontarget3 = 0
				else:
					target3 = 0
					nontarget3 = 1
				'''
				tt = (pa.g_name, pa.t_id, pa.chrm, str(pa.start), pa.strand, pa.tp,
					  str(sametoup), str(difftoup), str(sametodown), str(difftodown),
					  str(distal), str(proximal), str(middle), str(disttoup), str(disttodown),
					  str(distto3), str(distto5))
				'''
				tt = (pa.g_name, pa.t_id, pa.chrm, str(pa.start), pa.strand, pa.tp,
                                                                 str(sametoup), str(difftoup), str(sametodown), str(difftodown),
                                                                 str(distal), str(proximal), str(middle), str(disttoup), str(disttodown), 
								 str(distto3), str(distto5), str(target1), str(nontarget1), str(target2), 
								 str(nontarget2), str(target3), str(nontarget3))
				'''
				#pa_feat = PolyA_Feat(pa.g_name, pa.t_id, pa.chrm, pa.start, pa.strand, pa.tp,
				#				 sametoup, abs(1-sametoup), sametodown, abs(1-sametodown),
				#				 distal, proximal, middle, disttoup, disttodown,
				#				 target1, nontarget1, target2, nontarget2, target3, nontarget3)
				temp = "\t".join(map(str,tt[0:4])) + "\t" + tt[3] + "\t" + \
					   "\t".join(map(str,tt[4:])) + "\n"
				outfile.write(temp)

def get_35stopcodon_dict(threep, fivep, stopcodon):
	threep_dict = defaultdict(list)
	for line in threep:
		fds = line.split()
		chrm = fds[0]
		name = fds[3]
		start = fds[1]
		strand = fds[5]
		threep_dict[name].append(start)

	fivep_dict = defaultdict(list)
	for line in fivep:
		fds = line.split()
		chrm = fds[0]
		name = fds[3]
		start = fds[1]
		strand = fds[5]
		fivep_dict[name].append(start)

	scodon_dict = defaultdict(list)
	for line in stopcodon:
		fds = line.split()
		chrm = fds[0]
		name = fds[3]
		start = fds[1]
		strand = fds[5]
		scodon_dict[name].append(start)
	return threep_dict, fivep_dict, scodon_dict

def find_closest_feat(some_dict, polya):
	polya = PolyA._make(polya)
	if polya.t_id in some_dict:
		starts = some_dict[polya.t_id]
		dist = min(starts, key=lambda x: abs(int(x) - int(polya.start)))
		return abs(int(dist)-int(polya.start))
	else:
		print " not found in some dict" + polya.t_id
		return 0

def add_target_tag(target_list):
	tlist = []
	for line in target_list:
		fds = line.split()
		tlist.append(fds[2]+"="+fds[4])
	print tlist[1:5]
	return tlist

def add_target_nontarget(feature_file, target, out_file):
	feature_file = open(feature_file, 'r').read().splitlines()
	targetlist = open("/cbcl/forouzme/Projects/PolyA/lets-be-organized/Analysis/Plos/APA/published/labeled_15pct_"+ \
			  target + "_significantApAlogFCList.txt", 'r').read().splitlines()
	targets = add_target_tag(targetlist)
	outfile = open(out_file, 'w')
	outfile.write(feature_file[0]+ "\t" + target +"\n")
	for line in feature_file[1:]:
		fds = line.split()
		name = fds[1] + "=" + fds[2] + fds[5] + fds[3]
		if name in targets:
			outfile.write(line+ "\t" + "1" + "\n")
		else:
			outfile.write(line+ "\t" + "0" + "\n")

	outfile.close()


def run(gtf, bed, master, featfile):
	'''head
	This is based on mouse gtf file for now
	With ref seq gene and transcript ids
	I also used the master list provided previously
	In that masterlist there is just one transcript of each gene

	:return:
	'''
	if True :
		print " Lets start by reading the gtf file ... \n"
		gtf_file = open(gtf, 'r').read().splitlines()

		print " gtf file is loaded."
		print " Now we need to extract the 3 prime and 5 prime locations. \n"
		extract_3p_and_5p_fromgtf(gtf_file)
		threep = open('3prime.txt', 'r').read().splitlines()
		fivep = open('5prime.txt', 'r').read().splitlines()
		stopcodon = open('stopcodon.txt', 'r').read().splitlines()
		print " 3 prime and 5 prime Files are loaded back. I know it's stupid! \n"
		threep_dict, fivep_dict, scodon_dict = get_35stopcodon_dict(threep, fivep, stopcodon)

		print " I need the bed file just because it already has exon num"
		bedfile = open(bed, 'r').read().splitlines()
		fordist_dict , exon_dict = make_exon_dict(bedfile)

		print " And we are getting closer guys. Just let me load the master list!" \
			  "Hopefully you have already filtered out some noisy ones \n"
		masterfile = open(master, 'r').read().splitlines()
		polya_dict = parse_master_list(masterfile)
		print len(polya_dict)
		print " Master list is loaded nice and smooth \n"


	
		annotate(polya_dict, threep_dict, fivep_dict, fordist_dict, featfile)

	##TODO
	##I wanna add targets/ nontargets /part of target genes/ not part of target genes

if len(sys.argv) == 5:
	run(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
elif len(sys.argv) == 4:
	feature_file = sys.argv[1]
	target = sys.argv[2]
	outfile = sys.argv[3]
	add_target_nontarget(feature_file, target, outfile)



