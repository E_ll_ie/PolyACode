import sys
import os
import subprocess
import const
from shutil import copyfile
from shutil import move

def get_counts(bed12, name, genome):
	if genome == 'hg19':
		tophat_script = subprocess.call(['python ' + const.APAP + ' PARcounter ' + const.HG19_MASTERLIST +
										 ' ' +bed12 + ' ' + name ], shell=True)
	elif genome == 'mm9':
		tophat_script = subprocess.call(['python ' + const.APAP + ' PARcounter ' + const.MM9_MASTERLIST +
										 ' ' +bed12 + ' ' + name ], shell=True)


def get_all_counts(sfile, genome):
	name  = os.path.basename(sfile)
	sampleinfofile = open(sfile, 'r')
	outlist = []
	for line in sampleinfofile:
		bed12 = workspace+ "/bedfiles/"+ line.split()[0]
		name = line.split()[1]
		get_counts(bed12, name, genome)
		outlist.append('PARcounted_'+name+".txt")
		move('PARcounted_'+name+".txt", workspace+"/countfiles/")
	return outlist

def get_table(samplelist, table_name):
	copyfile(samplelist[0], 'temp.txt')
	for fl in samplelist[1:]:
		outfile = open(workspace+ "/tables/" +table_name, 'w')
		temp = open(table_name+'temp.txt', 'r').read().splitlines()
		current = open(fl, 'r').read().splitlines()
		for line_ind in range(len(temp)):
			new_line = temp[line_ind] + "\t" +  current[line_ind].split()[5] + "\n"
			outfile.write(new_line)
		current.close()
		outfile.close()
		copyfile(workspace+ "/tables/"+table_name, table_name+'temp.txt')
	os.remove(table_name+'temp.txt')

def get_sumtable(table_name):
	tophat_script = subprocess.call(['python ' + const.APAP + ' PARCSummer ' + workspace+ "/tables/"+ table_name],
									shell=True)
	move('summed_DGE_from_' + table_name, workspace+ "/tables/" )


def get_perctable(table_name) :
	tophat_script = subprocess.call(['python ' + const.APAP + ' rc2pct ' + table_name], shell=True)
	move('PCT_followed_' + table_name, workspace + "/tables/")

workspace=sys.argv[1]