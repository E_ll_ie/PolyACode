import sys
from collections import defaultdict
import subprocess

class Clip_Basic:
	def __init__(self):
		self.read_dict = defaultdict(list)
		self.barcodes ={ 'AATG' : 0 ,  'TGGC' : 1 ,  'GGCG': 2 ,  'AACC': 3  ,
						 'TTAG': 4 , 'CCAC': 5 }
		self.files = ['Mock.fastq', '27LacZ.fastq', 'D4-5.fastq', 'XL.fastq', 'KOS37KD.fastq', 'KOS63KD.fastq' ]

	def parse_fastq(self, fastq_f):
		fastq = open(fastq_f, 'r').read().splitlines()
		for line in fastq:
				if line.startswith("@"):
					read_id = line
				else:
					self.read_dict[read_id].append(line)

	def parse_aln_bed(self, bed_f, bed_o_dir):
		bed_dict = {}
		bed = open(bed_f, 'r').read().splitlines()
		for line in bed:
			fds = line.split()
			if fds[5] == "+":
				start = fds[1]
				end = str(int(fds[1]) + 1)
			else:
				end = fds[2]
				start = str(int(fds[2]) - 1)

			new_line = fds[0] + "\t" + start + "\t" + end + "\t" + fds[3] +\
					   "\t" + fds[4] + "\t" + fds[5]
			rnd_brcd = fds[3][-5:]

			tmp_key = fds[0] + "_" + start + "_" + fds[5] + "_" + rnd_brcd
			bed_dict[tmp_key] = new_line
		bed_o = open(bed_o_dir, 'w')
		for k in bed_dict:
			bed_o.write(bed_dict[k] + "\n")
		bed_o.close()

	def parse_and_separate(self, fastq_f):
		#fastq = open(fastq_f, 'r').read().splitlines()
		fastq = open(fastq_f, 'r')
		cur_files = []
		no_barcode_counter = 0
		c = 0
		for f in self.files:
			cur_files.append(open(f, 'w'))
		for line in fastq:
			if line.startswith('@SN'):
				if c != 0:
					r_str, brcd, fnd = self.parse_reads(r_info)
					if fnd:
						cur_files[self.barcodes[brcd]].write(r_str)
					if not fnd:
						no_barcode_counter +=1
				r_info = []
				r_info.append(line.strip())
				c += 1
				if c%1000000 == 0:
					print str(c) + " reads are done."
			else:
				r_info.append(line.strip())		
		r_str, brcd, fnd = self.parse_reads(r_info)
		if fnd:
			cur_files[self.barcodes[brcd]].write(r_str)
		if not fnd:
			no_barcode_counter += 1
		for f in cur_files:
			f.close()
		print no_barcode_counter

	def parse_reads(self, r_info):
		brcd = r_info[1][3:7]
		fnd = True
		#print r_info
		if brcd in self.barcodes:
			temp = r_info[0] + " " + r_info[1][0:3] + r_info[1][7:9] + "\n"
			temp += r_info[1][9:] + "\n"
			temp += r_info[2] + "\n"
			temp += r_info[3][9:] + "\n"
		else:
			fnd = False
			temp = ""
		return (temp,brcd,fnd)

	def separate_reads(self):
		cur_files = []
		no_barcode_counter = 0
		for f in self.files:
			cur_files.append(open(f, 'w'))
		for read in self.read_dict :
			r_info =  self.read_dict[read]
			brcd = r_info[0][3:7]
			if brcd in self.barcodes:
				temp = read + " " + r_info[0][0:3] + r_info[0][7:9] + "\n"
				temp += r_info[0][9:] + "\n"
				temp += r_info[1] + "\n"
				temp += r_info[2][9:] + "\n"
				cur_files[self.barcodes[brcd]].write(temp)

			else:
				no_barcode_counter +=1

		for f in cur_files:
			f.close()


	def run_bowtie_code(self, bwti_file):
		for fl in self.files:
			loadcode = subprocess.call('qsub /cbcl/forouzme/Projects/PolyA/CLIP/Script/run_bowtie.sh ' + fl , shell=True)
			print "Called Bowtie"



def main(bedfile, outfile):
	c = Clip_Basic()
	#c.parse_and_separate(fastq_file)
	# c.parse_fastq(fastq_file)
	# c.separate_reads()
	c.parse_aln_bed(bedfile, outfile)

main(sys.argv[1], sys.argv[2])
