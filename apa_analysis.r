# source the utils specifically designed for ApA splicing analysis
source('ApASAM_utils.R')

args <- commandArgs(trailingOnly = TRUE)

wd = args[1]
rc_table = args[2]
cond_f = args[3]
num_samples = args[4]
coeff = args[5]
outfile = args[6]

# setwd and load the library
setwd(wd)
library(edgeR)

x_raw <- load_counts_from(rc_table)
x_counts <- x_raw[,6:5+num_samples]  ## extract the int counts data
print(dim(x_counts))
myconditions <- set_experimental_conditions_as(cond_f)
#masterlist <- load_reference_from("ApA reference list if needed")
masterlist <- x_raw[,1:5] #will take the first two columns of the raw input as the reference.

# create DGEList object and filter by "minCPM" and "minDP"
y <- create_DGEList(x_counts, myconditions, masterlist)
y <- screen_row_on_miniumCPM_and_minimumDP(y, 2, 3)

# normalization
cat("normalizing the remaining counts...\n")
y <- calcNormFactors(y)

# generate MDS plot under current dir
generate_MDS(y, "MDS.pdf")

# define your experimental design and here I am adding an intercept column
cat("generating the experimental design...\n")
design <- model.matrix(~myconditions, data = y$samples) # ~with intercept coefficient
print(design)

# estimate the dispersion and fit model
cat("estimating NB dispersion....\n")
y <- estimateDisp(y, design, robust = TRUE)
cat("fitting glmQL model...\n")
fit <- glmQLFit(y, design, robust = TRUE)
cat("model fitting completed.")


sp <- diffSpliceDGE(fit, coef=coeff,  geneid = "geneSymbols", exonid = "ApAcoordinates")
#qlf <- glmQLFTest(fit, contrast = con)
#toplist <- topTags(qlf, n=25000)
toplist <- topSpliceDGE(sp, level = "exon", n=50000)
cat("writing output...\n")
# write.table(toplist, "Entirelist_U1-7_vscntrl.txt")
write.table(toplist, outfile)

cat("Done!\n")
