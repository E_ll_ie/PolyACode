import sys
import numpy as np
import scipy.io
from collections import defaultdict
import random
import const



def code(sequence):
	seqlist=[]
	for l in sequence[:]:
		if l=="a" or l=="A":
			seqlist.append([1,0,0,0])
		elif l=="c" or l=="C":
			seqlist.append([0,1,0,0])
		elif l=="g" or l=="G":
			seqlist.append([0,0,1,0])
		elif l=="t" or l=="T":
			seqlist.append([0,0,0,1])
		elif l=="n" or l=="N":
			seqlist.append([0,0,0,0])
		else:
			print "ERROR in SEQUENCE CODING " + l
			sys.exit(1)
	return np.array(seqlist)
	#return np.dstack(seqlist)

def get_complement(seq):
	complement = const.COMPLEMENT
	return "".join(complement.get(base, base) for base in reversed(seq))



def translate(infasta):
	in_fasta = open(infasta, 'r').read().splitlines()
	numofgenes=len(in_fasta)
	seqs, nums=[], []
	all_seq_to_check = []
	seqs_t, nums_t = [], []
	seqs_n, nums_n = [], []
	count = 0
	for ind in range(numofgenes):
		line = in_fasta[ind].split()
		seqq = in_fasta[ind].split()[25]
		## You need to know what you want to extract here
		t_raw = map(int, in_fasta[ind].split()[18:24])

		##You might want to put some condition here
		# if True:
		if (int(line[7]) == 0 or int(line[7]) > 200) and (int(line[8]) == 0 or int(line[8]) > 200):
			# tt = [t_raw[0], t_raw[1], t_raw[4], t_raw[5]]
			tt = [t_raw[4], t_raw[5]]
			# tt = [1]
			if len(seqq) < 401:
				diff = 401 - len(seqq)
				if diff % 2 == 0:
					seqq = "N" * (diff / 2) + seqq + "N" * (diff / 2)
				else:
					diff = diff - 1
					seqq = "N" * (diff / 2) + seqq + "N" * (diff / 2) + "N"
			# for small_seq in [seqq[0:250], seqq[50:300], seqq[100:350], seqq[150:400]]:
			for small_seq in [seqq[1:]]:
				codedseq = code(small_seq)
				upseqq = small_seq.upper()
				reverse_complement = "".join(const.COMPLEMENT.get(base, base) for base in reversed(upseqq))
				codedseq_r = code(reverse_complement)
				if upseqq not in all_seq_to_check and reverse_complement not in all_seq_to_check:
					if tt[0] == 1:
						tt = [1, 1]
						# tt = [1, 0, 0]
						# tt =[1]
						seqs_t.append(codedseq)
						nums_t.append(np.array(tt))
						seqs_t.append(codedseq_r)
						nums_t.append(np.array(tt))
						count += 2
					else:
						tt = [1, 0]
						# tt = [0, 1, 0]
						# tt = [0]
						seqs_n.append(codedseq)
						nums_n.append(np.array(tt))
						seqs_n.append(codedseq_r)
						nums_n.append(np.array(tt))
					all_seq_to_check.append(upseqq)


	target=np.array(nums)
	print len(seqs)
	train=np.array(seqs)

	c = list(zip(train, target))
	random.shuffle(c)
	train, target = zip(*c)

	#scipy.io.savemat('train_b4n.mat', mdict={"train":train[0:7000], "target":target[0:7000]})
	scipy.io.savemat('test_b4n_noc.mat', mdict={"train":train[:], "target":target[:]})
	#scipy.io.savemat('validate_b4n.mat', mdict={"train":train[7000:], "target":target[7000:]})
