'''
This module includes configurable constants
'''

import sys
import string

import numpy as np


'''
genome directories
'''
HG19_FASTA = ''
MM9_FASTA = ''


'''
chrom sizes
'''
HG19_CHR_SIZE = ''
MM9_CHR_SIZE = ''


'''
Comprehensive Annotation
For Tophat
'''
HG19_ANNOT_GTF = ''
MM9_ANNOT_GTF = ''

'''
Comprehensive Annotation
For MAJIQ
'''
HG19_ANNOT_GFF = ''
MM9_ANNOT_GFF = ''

'''
Masterlists - canonical
'''
HG19_MASTERLIST = ''
MM9_MASTERLIST = ''


'''
Masterlists - canonical+denovo
'''
HG19_MASTERLIST_MORE = ''
MM9_MASTERLIST_MORE = ''

'''
Other scripts
'''
APAP = '/cbcl/forouzme/Projects/PolyA/Repeat/Scripts/genome-wide-ApA-DE-project-in-UCI/apap.py'



'''
For finding complements
'''

COMPLEMENT = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
