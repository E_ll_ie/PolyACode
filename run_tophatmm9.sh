#!/bin/bash
#$ -N tophat
#$ -q cbcl-a64,cbcl-i24
#$ -pe openmp 8-64
#$ -m beas

module load bowtie2/2.2.7
module load tophat/2.1.0
export BOWTIE2_INDEXES=/cbcl/forouzme/Projects/PolyA/Repeat/Data/mm9_b2index/
Ref_Name=genome

input=$1
s=${input##*/}
name=${s%.*}
annot=/cbcl/forouzme/Projects/PolyA/Repeat/Data/mm9_annot/genes.gtf
workspace=$2
#tophat -p 32 -o /cbcl/forouzme/Projects/PolyA/Repeat/tophat_out/$name -g 1 --library-type fr-secondstrand -G $annot $Ref_Name $input

tophat -p 32 -o $workspace/tophat_out_fr_mm9/$name -g 1 --library-type fr-firststrand -G $annot $Ref_Name $input