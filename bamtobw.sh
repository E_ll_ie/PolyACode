#!/bin/bash
#$ -N GetBW
#$ -q cbcl-a64,pub64,free64
#$ -m beas

module load enthought_python

s1=$1
s1=${s1##*/}
name1=${s1%.*}
name1=$2
echo $name1

bamCoverage --bam $1 --binSize 1 --normalizeUsingRPKM --ignoreDuplicates -p 32 -of bigwig -o $name1.bw