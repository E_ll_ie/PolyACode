import sys
import subprocess
import os
import const
from collections import defaultdict #this will make your life simpler
from itertools import groupby
import string
import gzip
from itertools import islice

def run_tophat_single(reads, genome, workspace):
	name = os.path.basename(reads)
	print str(name) + "will be mapped."
	if genome == 'hg19':
		tophat_script = subprocess.call(['qsub run_tophathg19.sh ' + reads + " " + workspace], shell=True)
	elif genome == 'mm9':
		tophat_script = subprocess.call(['qsub run_tophatmm9.sh ' + reads + " " + workspace], shell=True)

def convert_to_bed12(bam, name):
	print "Converting to bed12"
	bedtools_load = subprocess.call(['module load bedtools'], shell=True)
	bed_script = subprocess.call(['bedtools bamtobed -i ' + bam +
								  ' -bed12  > ' + workspace + "/bedfiles/" + name + '.bed'], shell=True)

def convert_to_bam(bed12, name, genome):
	print "Converting to bam"
	if genome == 'mm9':
		chrsize = const.MM9_CHR_SIZE
	elif genome == 'hg19':
		chrsize = const.HG19_CHR_SIZE
	bedtools_load = subprocess.call(['module load bedtools'], shell=True)
	bed_script = subprocess.call(['bedtools bedtobam -i ' + bed12 +
								  ' -g ' + chrsize + '-bed12  > ' + workspace + "/bamfiles/" + name + '.bed'], shell=True)

def convert_bamtobw(bam, name):
	bedtools_load = subprocess.call(['sh bamtobw.sh ' + bam + " " + workspace + "/bwfiles/" + name ], shell=True)


"""
Used in filter_internals
"""
def fasta_iter(fasta_name):
	"""
    given a fasta file. yield tuples of header, sequence
    """
	d=dict()
	fh=fasta_name
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		header = header.next().split()[0][1:].strip()
		seq = "".join(s.strip() for s in faiter.next())
		#yield header, seq
		print header
		d[header]=seq
	return d


def filter_internals(bed12, genome):
	print "Filtering out reads with internal priming"
	name = os.path.basename(bed12)

	if genome == 'mm9':
		ref = open(const.MM9_FASTA ,'r')
	elif genome == 'hg19':
		ref = open(const.HG19_FASTA ,'r')

	bed12_aip = workspace + "/bedfiles/" + name + "_aip.bed"
	outfile = open(bed12_aip, 'w')
	count = 0;
	complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'U': 'A'}

	genome = fasta_iter(ref)

	for real_index, line in enumerate(open(bed12)):
		chrom, start, stop, gene, score, strand = line.strip().split('\t')[:6]
		stop = int(stop) - 1
		start = int(start) - 1
		if strand == '+':
			L = min(10, len(genome[chrom]) - int(stop))
			# print str(genome[chrom][int(stop):int(stop)+l])
			sequence = str(genome[chrom][int(stop) + 1:int(stop) + L + 1]).upper()
		else:
			L = min(10, start)
			n = str(genome[chrom][int(start) - L:int(start)]).upper()
			# print n
			sequence = "".join(complement.get(base, base) for base in reversed(n))
		# print sequence

		sequence = sequence.upper()
		if (sequence.find('AAAAAA') >= 0):
			# print sequence
			count += 1;
			flag = 1;
		elif (sequence.count('A') >= 7):
			# print sequence
			count += 1
			flag = 1;
		else:
			outfile.writelines(line);
	print str(count) + " were removed from " + str(name) + " in initial filtering."
	outfile.close()


def filter_tails(fastq, workspace):
	print "Filtering out reads with no polyA tail"
	N = 4
	name = os.path.basename(fastq)
	outname = workspace + "/trimmed_fastqs/" + name + "_trimmed.fastq"
	outfile = open(outname, 'w')
	count = 0;
	with open(fastq, 'r') as f:
		while True:
			next_n_lines = list(islice(f, N))
			if not next_n_lines:
				break
			else:
				# ['@DHCHSVN1:266:C4B7LACXX:7:1101:2185:1903 1:N:0:ATCACG\n',
				# 'GNCAACTCATCATTTTGTTCTTAAATAAAGACTGGGACACACAGTAGATAGCTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCACTTAAACCCCAC\n',
				# '+\n',
				# '@#1ADDFFHFFHHIJJJGEHIIJIJGIJIIFHJJJIFFEGCHHIIJIIJJJJIJJJIIIJHFDDDDDDDDDBB############################\n']
				seq = next_n_lines[1].split('\n')[0];
				idx = seq.find('AAAAAAAAAAAAAAA')
				if (idx == -1):
					count = count + 1
				elif (idx > 24):
					trimseq = seq[4:idx];
					name = next_n_lines[0].split(' ')[0] + "_" + seq[0:4];
					l3 = next_n_lines[2].split('\n')[0];
					l4 = next_n_lines[3].split('\n')[0][4:idx];
					outfile.write(name + "\n" + trimseq + "\n" + l3 + "\n" + l4 + "\n");
				else:
					count = count + 1;

	outfile.close()
	print str(count) + " were removed from " + str(name) + " in initial filtering."



workspace=sys.argv[1]


