#!/bin/bash
#$ -N tophat
#$ -q cbcl-a64,cbcl-i24
#$ -pe openmp 8-64
#$ -m beas

module load bowtie2/2.2.7
module load tophat/2.1.0
export BOWTIE2_INDEXES=/cbcl/forouzme/Projects/PolyA/Analysis2/Data/
Ref_Name=human19

input=$1
s=${input##*/}
name=${s%.*}
annot=/cbcl/forouzme/Projects/PolyA/RNASeq/Data/ensembl.hg19.gtf
workspace=$2
mkdir $workspace/tophat_out_fr_hg19/
##annot=/cbcl/forouzme/Projects/PolyA/Analysis2/Data/gencode.v19.annotation.gtf
#tophat -p 32 -o /cbcl/forouzme/Projects/PolyA/RNASeq/tophat_out/$name -g 1 --library-type fr-secondstrand -G $annot $Ref_Name $input

tophat -p 32 -o $workspace/tophat_out_fr_hg19/$name -g 1 --library-type fr-firststrand -G $annot $Ref_Name $input

